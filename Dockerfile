FROM maven:3.9.7-eclipse-temurin-21-alpine AS build
RUN mkdir -p /build
COPY  . /build
WORKDIR /build
RUN mvn -DskipTests clean install

FROM maven:3.9.7-eclipse-temurin-21-alpine
EXPOSE 8080
COPY --from=build /build/target/UnicomerTestMicroservice-0.0.1-SNAPSHOT.jar /src/app/UnicomerTestMicroservice-0.0.1.jar
COPY --from=build /build/src/main/resources/ /src/app/src/main/resources/
WORKDIR /src/app/
RUN mkdir -p src/app/temp
#RUN addgroup --system javauser && \
#    adduser -S -s /bin/false -G javauser javauser && \
#    chown -R javauser:javauser /src/app
#USER javauser
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "./UnicomerTestMicroservice-0.0.1.jar"]
