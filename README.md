# Proyecto Backend Unicomer

## Tools
* Spring boot
* Java 21
* Lombok

## Construcción de imagen dockerfile
### Comandos:
- $ docker build -t unicomer-test-microservice .
- $ docker run -p 8080:8080 unicomer-test-microservice

## Collection en Insomnia
- Insomnia_2024-06-01.json

## URL donde se desplego proyecto
- https://test-backend-unicomer.onrender.com