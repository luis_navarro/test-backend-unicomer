package com.luisnavarro.unicomertest.service;

import java.util.Set;

public interface XmlProcessingService {

	public Set<String> getDocumentationByXsd(String urlString) throws Exception;
	
	public Set<String> getSchemaLocationByXsd(String urlString) throws Exception;
}
