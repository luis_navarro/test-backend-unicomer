package com.luisnavarro.unicomertest.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.luisnavarro.unicomertest.service.XmlProcessingService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class XmlProcessingServiceImpl implements XmlProcessingService {
	
	private final @NonNull ResourceLoader resourceLoader;

	@Override
	public Set<String> getDocumentationByXsd(String urlString) throws Exception {
		Set<String> documentationSet = new HashSet<>();
		try {
        	
            // Cargar el archivo XML usando ResourceLoader
            Resource resource = resourceLoader.getResource("classpath:XMLSchema.xsd");
            InputStream inputStream = resource.getInputStream();

            // Crear un DocumentBuilder
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // Parsear el archivo XML
            Document document = builder.parse(inputStream);

            // Obtener la lista de nodos con la etiqueta <xs:documentation>
            NodeList nodeList = document.getElementsByTagName("xs:documentation");

            // Iterar sobre los nodos y extraer su contenido
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element element = (Element) nodeList.item(i);
                String documentationContent = element.getTextContent();
                if (Objects.nonNull(documentationContent) && StringUtils.hasText(documentationContent)) {
                	//System.out.println("Contenido de <xs:documentation>: " + documentationContent);
                	documentationSet.add(documentationContent);
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        
		return documentationSet;
	}

	@Override
	public Set<String> getSchemaLocationByXsd(String urlString) throws Exception {
		Set<String> schemaLocationSet = new HashSet<>();
		try {
        	
            // Cargar el archivo XML usando ResourceLoader
            Resource resource = resourceLoader.getResource("classpath:XMLSchema.xsd");
            InputStream inputStream = resource.getInputStream();

            // Crear un DocumentBuilder
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            // Parsear el archivo XML
            Document document = builder.parse(inputStream);

            // Obtener la lista de nodos con la etiqueta <xs:documentation>
            NodeList nodeList = document.getElementsByTagName("schemaLocation");

            // Iterar sobre los nodos y extraer su contenido
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element element = (Element) nodeList.item(i);
                String schemaLocationContent = element.getTextContent();
                if (Objects.nonNull(schemaLocationContent) && StringUtils.hasText(schemaLocationContent)) {
                	//System.out.println("Contenido de schemaLocation: " + schemaLocationContent);
                	schemaLocationSet.add(schemaLocationContent);
                }
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        
		return schemaLocationSet;
	}
}
