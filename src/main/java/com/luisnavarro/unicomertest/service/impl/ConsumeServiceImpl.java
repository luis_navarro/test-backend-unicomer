package com.luisnavarro.unicomertest.service.impl;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.luisnavarro.unicomertest.controller.dto.response.Response;
import com.luisnavarro.unicomertest.service.ConsumeService;
import com.luisnavarro.unicomertest.service.factory.Strategy;
import com.luisnavarro.unicomertest.service.wrapper.DocumentationWrapper;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ConsumeServiceImpl implements ConsumeService {
	
	private final List<Strategy> strategies;

	@Override
	public ResponseEntity<Response> findByAllDocumentation(String search) {
		
		DocumentationWrapper wrapper = DocumentationWrapper.builder()
				.searchDocumentation(search)
				.build();
		
		for (Strategy strategy : strategies) {
			strategy.perform(wrapper);
        }
		
		return ResponseEntity.ok(
				Response.builder()
				.message("Operación exitosa")
				.response(wrapper.getContentAllDocumentation())
				.build()
		);
	}

}
