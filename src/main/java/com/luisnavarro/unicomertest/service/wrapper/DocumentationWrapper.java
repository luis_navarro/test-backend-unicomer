package com.luisnavarro.unicomertest.service.wrapper;

import java.util.Set;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@Builder(toBuilder = true)
@EqualsAndHashCode(callSuper=false)
public class DocumentationWrapper {

	private String searchDocumentation;
	private String message;
	private Set<String> contentAllDocumentation;
}
