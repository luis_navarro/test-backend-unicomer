package com.luisnavarro.unicomertest.service.factory;

import com.luisnavarro.unicomertest.service.wrapper.DocumentationWrapper;

@FunctionalInterface
public interface Strategy {

	public void perform(DocumentationWrapper wrapper);
}
