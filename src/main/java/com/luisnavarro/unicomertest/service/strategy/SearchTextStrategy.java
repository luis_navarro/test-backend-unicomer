package com.luisnavarro.unicomertest.service.strategy;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.luisnavarro.unicomertest.service.XmlProcessingService;
import com.luisnavarro.unicomertest.service.factory.Strategy;
import com.luisnavarro.unicomertest.service.wrapper.DocumentationWrapper;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class SearchTextStrategy implements Strategy {

	private final @NonNull XmlProcessingService xmlProcessingService;

	@Value("${site.w3.base.url}")
	private String w3SitebaseUrl;

	@Override
	public void perform(DocumentationWrapper wrapper) {
		log.info("Executive Strategy SearchTextStrategy");
		if (Objects.nonNull(wrapper) && Objects.nonNull(wrapper.getSearchDocumentation())) {
			try {
				Set<String> content = xmlProcessingService.getDocumentationByXsd(w3SitebaseUrl);

				Set<String> documentationResult = content.stream()
						.filter(string -> string.contains(wrapper.getSearchDocumentation()))
						.collect(Collectors.toSet());

				wrapper.setContentAllDocumentation(documentationResult);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		log.info("Finish Strategy SearchTextStrategy");
	}
}
