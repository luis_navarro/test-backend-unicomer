package com.luisnavarro.unicomertest.service;

import org.springframework.http.ResponseEntity;

import com.luisnavarro.unicomertest.controller.dto.response.Response;

public interface ConsumeService {

	public ResponseEntity<Response> findByAllDocumentation(String search);
}
