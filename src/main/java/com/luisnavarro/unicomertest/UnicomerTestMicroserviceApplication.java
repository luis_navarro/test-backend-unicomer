package com.luisnavarro.unicomertest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnicomerTestMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UnicomerTestMicroserviceApplication.class, args);
	}

}
