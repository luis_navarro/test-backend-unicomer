package com.luisnavarro.unicomertest.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.luisnavarro.unicomertest.controller.dto.response.Response;
import com.luisnavarro.unicomertest.service.ConsumeService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/documentation")
@RequiredArgsConstructor
public class ConsumeSoapController {

	private final @NonNull ConsumeService consumeService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> getAllTextDocumentation(@RequestParam(required = false) String search) {
		return consumeService.findByAllDocumentation(search);
	}
}
