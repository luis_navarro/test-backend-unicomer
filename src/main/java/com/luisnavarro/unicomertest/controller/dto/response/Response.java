package com.luisnavarro.unicomertest.controller.dto.response;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@Builder(toBuilder = true)
@RequiredArgsConstructor
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Response implements Serializable {

	private static final long serialVersionUID = 1L;

	@NonNull
	private String message;

	private Object response;
}
