package com.luisnavarro.unicomertest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import com.luisnavarro.unicomertest.service.impl.XmlProcessingServiceImpl;

import lombok.RequiredArgsConstructor;

@SpringBootTest
@RequiredArgsConstructor
class UnicomerTestMicroserviceApplicationTests {
	
    @Value("${site.w3.base.url}")
    private String w3SitebaseUrl;
	
    @Autowired
    private XmlProcessingServiceImpl xmlProcessingService;

	@Test
	void contextLoads() {
		try {
            Set<String> content = xmlProcessingService.getDocumentationByXsd(w3SitebaseUrl);
            
            // Verificar que el conjunto no sea nulo
            assertNotNull(content, "El conjunto de contenido no debería ser nulo");

            // Verificar que el conjunto no esté vacío
            assertFalse(content.isEmpty(), "El conjunto de contenido debería tener valores");

            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

    @Test
    void testGetSearchDocumentation() {
		try {
            Set<String> content = xmlProcessingService.getDocumentationByXsd(w3SitebaseUrl);
            
			Set<String> documentationResult = content.stream()
					.filter(string -> string.contains("Texto que no debería estar"))
					.collect(Collectors.toSet());
            
            // Verificar que un texto específico no está en el conjunto (búsqueda no encontrada)
            String textNotFound = "Texto que no debería estar";
            assertTrue(documentationResult.isEmpty(), "El conjunto de contenido no debería contener el texto: " + textNotFound);

			documentationResult = content.stream()
					.filter(string -> string.contains("non-conflicting"))
					.collect(Collectors.toSet());
			
            // Verificar que un texto específico está presente en el conjunto
            String expectedText = "non-conflicting";
            assertTrue(!documentationResult.isEmpty(), "El conjunto de contenido debería contener el texto: " + expectedText);

            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
